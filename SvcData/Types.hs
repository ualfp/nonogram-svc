{-# LANGUAGE DeriveGeneric #-}

module SvcData.Types where

import Data.Aeson (FromJSON, ToJSON)
import Data.Text.Lazy (Text)
import GHC.Generics

data Input = Input {
  rowRuns :: [[Int]]
  , colRuns :: [[Int]]
  , blackCoords :: [(Int, Int)]
} deriving (Show, Generic)

instance ToJSON Input
instance FromJSON Input

data Output = Output {
  outMessage :: Text
  , outStatus :: Int
  , outGrid :: [[Int]]
} deriving (Show, Generic)

instance ToJSON Output
instance FromJSON Output
