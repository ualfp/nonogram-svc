module Solver.Solver where

import Solver.Grid
import SvcData.Types

import Data.List
import Data.Vector (Vector)
import qualified Data.Vector as V

nonogramSolver :: Input -> Grid
nonogramSolver p = fooBar (rowRuns p) (colRuns p) $ startGrid (length $ rowRuns p) (blackCoords p)

{- TODO: free space calculation needs to take into effect line direction-}
freeSpace :: Int -> [Int] -> Int
freeSpace size l = size - sum l - (length l - 1)

-- returns all possible permutations of l things taken fs at a time where l
-- things add up to l
combo :: Int -> Int -> [[Int]]
combo fs 1 = [[fs]]
combo 0 l = [replicate l 0]
combo fs l = [ x:cs |  x <- [0 .. fs]
                    , cs <- combo (fs-x) (l-1)
                          ]

-- gives the White possibilities for a black run
baseWhites :: Int -> [Int] -> [[Int]]
baseWhites size black = combo fs l
  where
    fs = freeSpace size black
    l = length black + 1

-- corrects the white possibilities by 1 in the middle gaps (because there is
-- a minimum of 1 white between black runs)
whites :: [[Int]] -> [[Int]]
whites = map minWhite
  where
    minWhite (w:ws) = start : map (1+) body ++ end
      where
        l = length ws
        start = w
        body = take (l - 1) ws
        end = drop (l - 1) ws
    minWhite [] = []

-- takes a line of the grid and list of Int (which define black runs)
-- determines all possibilities of runs that can be put in the grid line,
-- based on what is already there. It then places whites and blacks
-- where they must be. Unknowns are padded in when it isn't definitely a white or black
definiteChoices :: Int -> Vector Block -> [Int] -> [(Block, Int)]
definiteChoices size gl black = foldr1 partialApply choices
  where
    choices = filter filterConsistent $ map (joinLine black) $ whites $ baseWhites size black
    joinLine _ [w] = [(W, w) | w > 0]
    joinLine (b:bs) (w:ws) = [(W, w) | w > 0] ++ [(B, b)] ++ joinLine bs ws
    joinLine _ _ = []
    filterConsistent l = consistent gl $ runToLine l

-- Takes the grid and a line and returns a list of block runs
-- of W, B, and U. Ws & Bs are put in where they certain to be.
foo :: Int -> Grid -> Line -> [(Block, Int)]
foo size g l = definiteChoices size (readGridLine size i d g) r
  where
    i = index l
    d = lDir l
    r = blackRuns l

bar :: [[Int]] -> [[Int]] -> Grid -> Grid
bar rs cs g = foldl' (\acc y -> writeGridLine size (thisLine y acc) acc) g $ allData rs cs
  where
    size = (length rs)
    thisLine x a = Line (index x) (lDir x) (blackRuns x) (runToLine $ foo size a x)

fooBar :: [[Int]] -> [[Int]] -> Grid -> Grid
fooBar rs cs g
  | U `elem` g = fooBar rs cs $ bar rs cs g
  | otherwise  = g

--converts a list of different block runs into a "line"
runToLine :: [(Block, Int)] -> Vector Block
runToLine = foldr ((V.++) . runs) V.empty

--combines two different block runs into one which is consistent with both
partialApply :: [(Block, Int)] -> [(Block, Int)] -> [(Block, Int)]
partialApply [] [] = []
partialApply ((b, l):bs) ((b', l'):bs')
  | b == b' && l == l' = (b, l)   : partialApply bs bs'
  | b == b' && l < l'  = (b, l)   : partialApply bs ((b', l' - l):bs')
  | b == b' && l > l'  = (b, l')  : partialApply ((b, l - l'):bs) bs'
  -- if it gets here, they are equal to each other, so slap in an Unknown
  |            l == l' = (U, l)   : partialApply bs bs'
  |            l < l'  = (U, l)   : partialApply bs ((b', l' - l):bs')
  |            l > l'  = (U, l')  : partialApply ((b, l - l'):bs) bs'
partialApply _ _ = []

allData :: [[Int]] -> [[Int]] -> [Line]
allData rs cs = sortOn ((freeSpace (length rs)) . Solver.Grid.blackRuns) $ rowsL ++ colsL
  where
    rowsL = zipWith (\i r -> makeLine i Row r) [0..] rs
    colsL = zipWith (\i c -> Line i Col c V.empty) [0..] cs
