module Solver.Grid where

import Data.Vector as V
import Data.List as L

data Block = B | W | U

data Line = Line {
  index :: Int,
  lDir :: Dir,
  blackRuns :: [Int],
  line :: Vector Block
} deriving (Show)

data Dir = Row | Col deriving (Eq, Show)

type Grid = Vector Block

type NewLine = Vector Block
type NewGrid = Vector NewLine

instance Show Block where
  show B = "B"
  show W = "W"
  show U = "?"

instance Eq Block where
  B == B = True
  W == W = True
  U == U = True
  _ == _ = False

instance Ord Block where
  compare B W = GT
  compare W B = LT
  compare _ _ = EQ

gridIndex :: Int -> Line -> [Int]
gridIndex size l
  | lDir l == Row = L.map (\x -> index l * size + x) [0..(size - 1)]
  | otherwise     = L.map (\x -> x * size + index l) [0..(size - 1)]

blockCheck :: Block -> Block -> Bool
blockCheck _ U = True
blockCheck U _ = True
blockCheck inBlock gridBlock = inBlock == gridBlock

consistent :: Vector Block -> Vector Block -> Bool
consistent l l' = V.and $ V.zipWith blockCheck l l'

startGrid :: Int -> [(Int, Int)] -> Grid
startGrid size bs = writeGridB size bs $ blankGrid size

blankGrid :: Int -> Grid
blankGrid size = V.replicate (size * size) U

writeGridB :: Int -> [(Int, Int)] -> Grid -> Grid
writeGridB size coords = flip (V.//) $ indexesB $ gridIndexes size coords

gridIndexes :: Int -> [(Int, Int)] -> [Int]
gridIndexes size = Prelude.map $ \(r, c) -> r * size + c

indexesB :: [Int] -> [(Int, Block)]
indexesB = flip Prelude.zip $ repeat B

makeLine :: Int -> Dir -> [Int] -> Line
makeLine i d is = Line i d is V.empty

runs :: (Block, Int) -> Vector Block
runs (b, i) = V.replicate i b

writeGridLine :: Int -> Line -> Grid -> Grid
writeGridLine size l g = g V.// L.zip (gridIndex size l) (V.toList (line l))

newWriteGridLine :: Line -> NewGrid -> NewGrid
newWriteGridLine l g
  | lDir l == Row = g V.// [(index l, line l)]
  | otherwise     = undefined

readGridBlock :: Int -> (Int, Int) -> Grid -> Block
readGridBlock size (r, c) = flip (V.!) $ r * size + c

readGridLine :: Int -> Int -> Dir -> Grid -> Vector Block
readGridLine size i Row g = V.generate size (\x -> readGridBlock size (i, x) g)
readGridLine size i Col g = V.generate size (\x -> readGridBlock size (x, i) g)
