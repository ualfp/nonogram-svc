# Nonogram-svc

## Overview
A Haskell web service to solve [nonogram][] puzzles. We'll use the [Scotty][] web framework and [Aeson][] for this exercise.

## Running the service
post a request to the web service with cURL like so:
```bash
curl -d @samples/req.json http://localhost:3000/nonogram```


[nonogram]: https://en.wikipedia.org/wiki/Nonogram
[Scotty]: https://hackage.haskell.org/package/scotty
[Aeson]: https://hackage.haskell.org/package/aeson
