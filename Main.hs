{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Data.Monoid ((<>))
import qualified Data.Vector as V
import Network.HTTP.Types.Status
import Network.Wai.Middleware.RequestLogger

import SvcData.Types
import qualified Solver.Grid as G
import qualified Solver.Solver as S

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  get "/echo/:word" $ do
    echo <- param "word"
    html $ "<h1>" <> echo <> "</h1>"
  post "/nonogram" $ do
    x <- jsonData `rescue` const next
    json $ solve x
  post "/nonogram" $ do
    json err
    status badRequest400

solve :: Input -> Output
solve v = Output {
  outMessage = "success"
  , outStatus = 200
  , outGrid = toList (length $ rowRuns v) (S.nonogramSolver v)
}

err :: Output
err = Output {
  outMessage = "Invalid request"
  , outStatus = 400
  , outGrid = []
}

toList :: Int -> G.Grid -> [[Int]]
toList rows g = part (map blockToInt (V.toList g)) rows []  where
  part [] _ acc = acc
  part xs n acc = part (drop n xs) n (acc ++ [take n xs])

blockToInt :: G.Block -> Int
blockToInt G.B = 1
blockToInt _ = 0
